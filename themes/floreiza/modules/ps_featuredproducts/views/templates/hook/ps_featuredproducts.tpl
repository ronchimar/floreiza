<section class="featured-products clearfix">
  <!--
  <h1 class="h1 products-section-title text-uppercase ">
    {l s='Popular Products' d='Shop.Theme.Catalog'}
  </h1>
  -->
  <div class="products row">
    {foreach from=$products item="product"}
      	{include file="catalog/_partials/miniatures/product.tpl" product=$product}
    {/foreach}
  </div>
  <div class="all-product-link-container">
  	<a class="all-product-link" href="{$allProductsLink}">
	    {l s='All products' d='Shop.Theme.Catalog'}<i class="fa fa-angle-double-right" aria-hidden="true"></i>
	  </a>
  </div>
  
</section>