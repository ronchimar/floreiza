{function name="menu" nodes=[] depth=0}
  {strip}
    {if $nodes|count}
      <!-- main desktop menu -->
        {foreach from=$nodes item=node}
            <a href="{$node.url}" {if $node.open_in_new_window} target="_blank" {/if}>{$node.label}</a>
        {/foreach}
        	<a href="" title="Floreiza shop">shop</a>
          <a href="" title="Floreiza lookbook">lookbook</a>
          <a href="" title="Floreiza stores">stores</a>
          <a href="" title="Floreiza news">news</a>
    {/if}
  {/strip}
{/function}

<div class="menu">
    {menu nodes=$menu.children}
</div>
