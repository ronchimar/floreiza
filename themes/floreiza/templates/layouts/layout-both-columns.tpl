{**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
<!doctype html>
<html lang="{$language.iso_code}">

  <head>
    {block name='head'}
      {include file='_partials/head.tpl'}
    {/block}
  </head>

  <body id="{$page.page_name}" class="{$page.body_classes|classnames}">

    {hook h='displayAfterBodyOpeningTag'}

    <main>
    	<div class="container">
      {block name='product_activation'}
        {include file='catalog/_partials/product-activation.tpl'}
      {/block}
      <header id="header">
        {block name='header'}
          {include file='_partials/header.tpl'}
        {/block}
      </header>
      {block name='notifications'}
        {include file='_partials/notifications.tpl'}
      {/block}
      <section>
        <div class="container">

          {block name="left_column"}
            <div id="left-column" class="col-xs-12 col-sm-4 col-md-3">
              {if $page.page_name == 'product'}
                {hook h='displayLeftColumnProduct'}
              {else}
                {hook h="displayLeftColumn"}
              {/if}
            </div>
          {/block}

          {block name="content_wrapper"}
            <div id="content-wrapper" class="left-column right-column col-sm-4 col-md-6">
              {block name="content"}
                <p>Hello world! This is HTML5 Boilerplate.</p>
              {/block}
            </div>
          {/block}

          {block name="right_column"}
            <div id="right-column" class="col-xs-12 col-sm-4 col-md-3">
              {if $page.page_name == 'product'}
                {hook h='displayRightColumnProduct'}
              {else}
                {hook h="displayRightColumn"}
              {/if}
            </div>
          {/block}
        </div>
      </section>

	<!-- bottom divider -->  
    <div class="container">
    	<section class="bottom-divider">

    	</section>
    </div>
    

	</div><!-- /.container -->
	
	<footer id="footer" class="grey-background">
		      <div class="container">
		          <div class="row footer-box">
		              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 footer-links">
		                  <a href="" title="Contatti">Contatti</a>
		                  <a href="" title="Franchising">Franchising</a>
		                  <a href="" title="Azienda">Azienda</a>
		              </div>
		              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 footer-links">
		                  <a href="" title="Condizioni generali">Condizioni generali</a>
		                  <a href="" title="Modalità di spedizione">Modalità di spedizione</a>
		                  <a href="" title="Modalità di pagamento">Modalità di pagamento</a>
		                  <a href="" title="Resi e rimborsi">Resi e rimborsi</a>
		              </div>              
		              <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 footer-links to-right">
		                  <p>Copyright &copy; Floreiza</p>
		                      <p>P.IVA 02022700971</p>
		                          <a href="" title="Privacy policy">Privacy policy</a>
		              </div>
		          </div>
		          
		          
		      </div>
	</footer>
	<!--
	<footer id="footer">
        {*
        {block name="footer"}
          {include file="_partials/footer.tpl"}
        {/block}
        *}
    </footer>
	-->
	</main>
	
	
    {block name='javascript_bottom'}
      {include file="_partials/javascript.tpl" javascript=$javascript.bottom}
    {/block}

    {hook h='displayBeforeBodyClosingTag'}

  </body>

</html>
