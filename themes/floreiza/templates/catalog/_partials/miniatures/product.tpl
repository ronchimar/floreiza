<article class="col-md-4 product-miniature js-product-miniature product-home-floreiza" data-id-product="{$product.id_product}" data-id-product-attribute="{$product.id_product_attribute_floreiza}" itemscope itemtype="http://schema.org/Product">
  <div class="thumbnail-container">
    {block name='product_thumbnail'}
      <a href="{$product.url}" class="thumbnail product-thumbnail">
        <img
          src = "{$product.cover.bySize.home_default.url}"
          alt = "{$product.cover.legend}"
          data-full-size-image-url = "{$product.cover.large.url}"
        >
      </a>
    {/block}

    <div class="product-description">
      {block name='product_name'}
        <h2 class="product-title" itemprop="name"><a href="{$product.url}">{$product.name|truncate:50:'...'}</a></h2>
      {/block}
      
      {block name='product_price_and_shipping'}
        {if $product.show_price}
          <div class="product-price-and-shipping">
            {if $product.has_discount}
              {hook h='displayProductPriceBlock' product=$product type="old_price"}

              <span class="regular-price">{$product.regular_price}</span>
              {if $product.discount_type === 'percentage'}
                <span class="discount-percentage">{$product.discount_percentage}</span>
              {/if}
            {/if}

            {hook h='displayProductPriceBlock' product=$product type="before_price"}

            <span itemprop="price" class="price">{$product.price}<span class="price_iva_inclusa"> IVA inc.</span></span>

            {hook h='displayProductPriceBlock' product=$product type='unit_price'}

            {hook h='displayProductPriceBlock' product=$product type='weight'}
          </div>
        {/if}
      {/block}
    </div>
    {block name='product_flags'}
      <ul class="product-flags">
        {foreach from=$product.flags item=flag}
          <li class="{$flag.type}">{$flag.label}</li>
        {/foreach}
      </ul>
    {/block}
    <div class="highlighted-informations{if !$product.main_variants} no-variants{/if} hidden-sm-down">
      <div class="row">
      	<a
	        href="#"
	        class="quick-view col-md-6"
	        data-link-action="quickview"
	      >
      	<i class="fa fa-search" aria-hidden="true" title="{l s='Quick view'}"></i>
         {l d='Shop.Theme.Actions'}
      	</a>
      	
      	<a
	        href="#"
	        class="quick-view col-md-6"
	        data-link-action="quickview"
	      >
	      <i class="fa fa-star-o" aria-hidden="true" title="{l s='Add to whislist'}"></i>
	         {l d='Shop.Theme.Actions'}
	      </a>
      </div>
      
      
      

      {block name='product_variants'}
        {if $product.main_variants}
          {include file='catalog/_partials/variant-links.tpl' variants=$product.main_variants}
        {/if}
      {/block}
    </div>

  </div>
</article>
