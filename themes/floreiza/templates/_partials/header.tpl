{block name='header_top'}

  <div class="header-top">
    <div class="container">
    	
    	<!-- top header -->    
          <div class="row top-header-box">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 to-left">
	       	  {hook h='displayNav1'}
            </div>
            <!-- Controlli utente (lingua, carrello, valuta, login) -->
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 to-right display_nav">
              {hook h='displayNav'}
            </div>             
          </div>
      
      <!-- social header -->    
          <div class="row top-header-box">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 to-left">
              &nbsp;
            </div>   
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 to-right">
              sociala icons
            </div>             
          </div>      
      
      <!-- logo header -->
      <div class="header-logo-box" id="_desktop_logo">
	  	<a href="{$urls.base_url}" title="Floreiza Shop"><img src="{$shop.logo}" alt="{$shop.name}"/></a>
      </div>
      

	  <!-- main desktop menu -->
      
      <div class="main-desktop-menu-box">
          <a href="" title="Floreiza shop">shop</a>
          <a href="" title="Floreiza lookbook">lookbook</a>
          <a href="" title="Floreiza stores">stores</a>
          <a href="" title="Floreiza news">news</a>
      </div>
        
    
      <div id="mobile_top_menu_wrapper" class="row hidden-md-up" style="display:none;">
        <div class="js-top-menu mobile" id="_mobile_top_menu"></div>
        <div class="js-top-menu-bottom">
          <div id="_mobile_currency_selector"></div>
          <div id="_mobile_language_selector"></div>
          <div id="_mobile_contact_link"></div>
        </div>
      </div>
  	
  		
      <div class="row breadcrumb_search">
      	<!-- breadcumb -->     
	  	  {block name='breadcrumb'}
	        {include file='_partials/breadcrumb.tpl'}
	      {/block}
	      
	      <div class="col-sm-6">
	      	{widget name="ps_searchbar"}
	      </div>
      </div> 
  	  
	  <!-- submenu -->
	  <div class="main-page-menu">
	  <!-- Hook displayTop (Menu categorie)-->
	        {*{hook h='displayTop'}*}
	 		  <a href="/it/3-abiti" title="">Abiti</a>
	          <a href="" title="">Camicie</a> 
	          <a href="" title="">Cappotti</a> 
	          <a href="" title="">Casacche</a> 
	          <a href="" title="">Fasce Giacche</a>
	          <a href="" title="">Giubbotti</a>
	          <a href="" title="">Gilet</a>
	          <a href="" title="">Gonne</a>
	          <a href="" title="">Maglie</a>
	          <a href="" title="">Pantaloni & Jeans</a>
	          <a href="" title="">Salopette & Tute</a>
	          <a href="" title="">T-shirt Top</a>
	  </div> 
	  
	  <div class="main-desktop-menu-box">
	  
	  </div>
  	</div><!-- / .container -->
  </div>
{/block}






