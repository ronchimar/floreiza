<nav data-depth="{$breadcrumb.count}" class="hidden-sm-down col-sm-6">
  <ol class="breadcrumb">
  	<li class="breadcrumb-item">
  		<a itemprop="item" href="http://www.floreiza.it" >
          <span itemprop="name">Home</span>
       </a>
  	</li>
    {foreach from=$breadcrumb.links item=path name=breadcrumb}
      <li class="breadcrumb-item">
        <a href="{$path.url}">
          <span>{$path.title}</span>
        </a>
      </li>
    {/foreach}
  </ol>
</nav>
