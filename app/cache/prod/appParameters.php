<?php return array (
  'parameters' => 
  array (
    'database_host' => 'localhost',
    'database_port' => '',
    'database_name' => 'prestashop17',
    'database_user' => 'root',
    'database_password' => 'root',
    'database_prefix' => 'ps_',
    'database_engine' => 'InnoDB',
    'mailer_transport' => 'smtp',
    'mailer_host' => '127.0.0.1',
    'mailer_user' => NULL,
    'mailer_password' => NULL,
    'secret' => 'zPRJDM8g0nriQTefo4OfJdzLRii3AfUPIHvIcyfQV6TH6GGoosjeCZbg',
    'ps_caching' => 'CacheMemcache',
    'ps_cache_enable' => false,
    'ps_creation_date' => '2017-01-13',
    'locale' => 'it-IT',
    'cookie_key' => 'jIfdk9RJ06g13JO5oNg3hVManFge5i97SCbwdcak3rBz47VCpmU5z3ih',
    'cookie_iv' => 's28s2KLo',
    'new_cookie_key' => 'def0000035acaee1041a28f1306846f9fb8f4d7e62346f37fcbffc6977cba53077552988ffac6c1c5ecfb89697080dcd82c5e2cef433970dd0040756f3be88fd3cfa8d60',
  ),
);
