<?php

/* __string_template__3c395f5bcf0e015b8b622469e07bb427ea2d3d2d7cede17dddaa63e624e3843d */
class __TwigTemplate_76d3352d4f5f3e83815d520b85acc4ecd39ed4514bf01eedf1e3f7b4fde844c5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'extra_stylesheets' => array($this, 'block_extra_stylesheets'),
            'content_header' => array($this, 'block_content_header'),
            'content' => array($this, 'block_content'),
            'content_footer' => array($this, 'block_content_footer'),
            'sidebar_right' => array($this, 'block_sidebar_right'),
            'javascripts' => array($this, 'block_javascripts'),
            'extra_javascripts' => array($this, 'block_extra_javascripts'),
            'translate_javascripts' => array($this, 'block_translate_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"it\">
<head>
  <meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=0.75, maximum-scale=0.75, user-scalable=0\">
<meta name=\"apple-mobile-web-app-capable\" content=\"yes\">
<meta name=\"robots\" content=\"NOFOLLOW, NOINDEX\">

<link rel=\"icon\" type=\"image/x-icon\" href=\"/img/favicon.ico\" />
<link rel=\"apple-touch-icon\" href=\"/img/app_icon.png\" />

<title>Prodotti • Floreiza</title>

  <script type=\"text/javascript\">
    var help_class_name = 'AdminProducts';
    var iso_user = 'it';
    var full_language_code = 'it';
    var full_cldr_language_code = 'it-IT';
    var country_iso_code = 'IT';
    var _PS_VERSION_ = '1.7.0.4';
    var roundMode = 2;
    var youEditFieldFor = '';
        var new_order_msg = 'È stato effettuato un nuovo ordine nel tuo negozio.';
    var order_number_msg = 'Numero dell\\\\\\'ordine: ';
    var total_msg = 'Totale: ';
    var from_msg = 'Da: ';
    var see_order_msg = 'Vedi quest\\\\\\'ordine';
    var new_customer_msg = 'Un nuovo cliente si è registrato nel tuo negozio.';
    var customer_name_msg = 'Nome cliente: ';
    var new_msg = 'Al tuo negozio è stato inviato un nuovo messaggio.';
    var see_msg = 'Leggi questo messaggio';
    var token = 'c5a5f43b25d6657755557bb1cfc8ad85';
    var token_admin_orders = 'f6fe8a7d701144a1b39a62b5b3fb5ae8';
    var token_admin_customers = '9a3f4deace6da3ca7c83eaa0953f7340';
    var token_admin_customer_threads = '439b0d380fa730ae91f1994ec90c2e0b';
    var currentIndex = 'index.php?controller=AdminProducts';
    var employee_token = 'd666f957b501390be2d8425d46a683b2';
    var choose_language_translate = 'Scegli lingua';
    var default_language = '1';
    var admin_modules_link = '/admin631zqgp8w/index.php/module/catalog/recommended?_token=i4OrJhR5MTqS-LCyip5PoZFsrjZ6ELNFqMiE4MJ5NoQ';
    var tab_modules_list = 'prestagiftvouchers,dmuassocprodcat,etranslation,apiway,prestashoptoquickbooks';
    var update_success_msg = 'Aggiornato con successo';
    var errorLogin = 'PrestaShop non ha potuto accedere ad Addons. Si prega di controllare le tue credenziali e la tua connessione Internet.';
    var search_product_msg = 'Cerca un prodotto';
  </script>

      <link href=\"/admin631zqgp8w/themes/new-theme/public/theme.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/js/jquery/plugins/chosen/jquery.chosen.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/admin631zqgp8w/themes/default/css/vendor/nv.d3.css\" rel=\"stylesheet\" type=\"text/css\"/>
  
  <script type=\"text/javascript\">
var baseAdminDir = \"\\/admin631zqgp8w\\/\";
var baseDir = \"\\/\";
var currency = {\"iso_code\":\"EUR\",\"sign\":\"\\u20ac\",\"name\":\"Euro\",\"format\":\"#,##0.00\\u00a0\\u00a4\"};
var host_mode = false;
</script>
<script type=\"text/javascript\" src=\"/admin631zqgp8w/themes/new-theme/public/bundle.js\"></script>
<script type=\"text/javascript\" src=\"/js/jquery/plugins/jquery.chosen.js\"></script>
<script type=\"text/javascript\" src=\"/js/admin.js?v=1.7.0.4\"></script>
<script type=\"text/javascript\" src=\"/js/cldr.js\"></script>
<script type=\"text/javascript\" src=\"/js/tools.js?v=1.7.0.4\"></script>
<script type=\"text/javascript\" src=\"/admin631zqgp8w/public/bundle.js\"></script>
<script type=\"text/javascript\" src=\"/js/vendor/d3.v3.min.js\"></script>
<script type=\"text/javascript\" src=\"/admin631zqgp8w/themes/default/js/vendor/nv.d3.min.js\"></script>


  

";
        // line 69
        $this->displayBlock('stylesheets', $context, $blocks);
        $this->displayBlock('extra_stylesheets', $context, $blocks);
        echo "</head>
<body class=\"adminproducts\">



<header>
  <nav class=\"main-header\">

    
    

    
    <a class=\"logo pull-left\" href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminDashboard&amp;token=a425aecb4ed34fae2b99347b22f7dde7\"></a>

    <div class=\"component pull-left\"><div class=\"ps-dropdown dropdown\">
  <span type=\"button\" id=\"quick-access\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
    <span class=\"selected-item\">Accesso Veloce</span> <i class=\"material-icons arrow-down\">keyboard_arrow_down</i>
  </span>
  <div class=\"ps-dropdown-menu dropdown-menu\" aria-labelledby=\"quick-access\">
          <a class=\"dropdown-item\"
         href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminCategories&amp;addcategory&amp;token=ff9f9cd4e863de4abd0b025a8bd9bc4c\"
                 data-item=\"Nuova categoria\"
      >Nuova categoria</a>
          <a class=\"dropdown-item\"
         href=\"http://localhost/admin631zqgp8w/index.php/productnew?token=1bdd2a1f502eb931064a6ddecf79f389\"
                 data-item=\"Nuovo prodotto\"
      >Nuovo prodotto</a>
          <a class=\"dropdown-item\"
         href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminCartRules&amp;addcart_rule&amp;token=360d277e72b74d8e00ad22c0b40ceceb\"
                 data-item=\"Nuovo voucher\"
      >Nuovo voucher</a>
          <a class=\"dropdown-item\"
         href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminOrders&amp;token=f6fe8a7d701144a1b39a62b5b3fb5ae8\"
                 data-item=\"Orders\"
      >Orders</a>
        <hr>
        <a
      class=\"dropdown-item js-quick-link\"
      data-rand=\"109\"
      data-icon=\"icon-AdminCatalog\"
      data-method=\"add\"
      data-url=\"index.php/productform1?-LCyip5PoZFsrjZ6ELNFqMiE4MJ5NoQ\"
      data-post-link=\"http://localhost/admin631zqgp8w/index.php?controller=AdminQuickAccesses&token=b4f510edc0cf45411b9e0b4e0facd634\"
      data-prompt-text=\"Da\\\\\\' un nome a questo shortcut:\"
      data-link=\"Prodotti - Lista\"
    >
      <i class=\"material-icons\">add_circle_outline</i>
      Aggiungi a QuickAccess la pagina corrente
    </a>
    <a class=\"dropdown-item\" href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminQuickAccesses&token=b4f510edc0cf45411b9e0b4e0facd634\">
      <i class=\"material-icons\">settings</i>
      Gestisci gli accessi rapidi
    </a>
  </div>
</div>
</div>
    <div class=\"component\">

<form id=\"header_search\"
      class=\"bo_search_form dropdown-form js-dropdown-form\"
      method=\"post\"
      action=\"/admin631zqgp8w/index.php?controller=AdminSearch&amp;token=c61c82e9b54b68debf8613d2ddd40a2d\"
      role=\"search\">
  <input type=\"hidden\" name=\"bo_search_type\" id=\"bo_search_type\" class=\"js-search-type\" />
    <div class=\"input-group\">
    <input id=\"bo_query\" name=\"bo_query\" type=\"search\" class=\"form-control dropdown-form-search js-form-search\" value=\"\" placeholder=\"Ricerca (es. riferimento prodotto, nome cliente...)\" />
    <div class=\"input-group-addon\">
      <div class=\"dropdown\">
        <span class=\"dropdown-toggle js-dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\">
          Ovunque
        </span>
        <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenu\">
          <ul class=\"items-list js-items-list\">
            <li class=\"search-all search-option active\">
              <a class=\"dropdown-item\" data-item=\"Ovunque\" href=\"#\" data-value=\"0\" data-placeholder=\"Cosa hai bisogno di trovare?\" data-icon=\"icon-search\">
              <i class=\"material-icons\">search</i> Ovunque</a>
            </li>
            <hr>
            <li class=\"search-book search-option\">
              <a class=\"dropdown-item\" data-item=\"Catalogo\" href=\"#\" data-value=\"1\" data-placeholder=\"Nome prodotto, codice a barre, riferimento...\" data-icon=\"icon-book\">
                <i class=\"material-icons\">library_books</i> Catalogo
              </a>
            </li>
            <li class=\"search-customers-name search-option\">
              <a class=\"dropdown-item\" data-item=\"Clienti per nome\" href=\"#\" data-value=\"2\" data-placeholder=\"E-mail, nome, ...\" data-icon=\"icon-group\">
                <i class=\"material-icons\">group</i> Clienti per nome
              </a>
            </li>
            <li class=\"search-customers-addresses search-option\">
              <a class=\"dropdown-item\" data-item=\"Clienti per indirizzo IP\" href=\"#\" data-value=\"6\" data-placeholder=\"123.45.67.89\" data-icon=\"icon-desktop\">
                <i class=\"material-icons\">desktop_windows</i>Clienti per indirizzo IP</a>
            </li>
            <li class=\"search-orders search-option\">
              <a class=\"dropdown-item\" data-item=\"Ordini\" href=\"#\" data-value=\"3\" data-placeholder=\"ID ordine\" data-icon=\"icon-credit-card\">
                <i class=\"material-icons\">credit_card</i> Ordini
              </a>
            </li>
            <li class=\"search-invoices search-option\">
              <a class=\"dropdown-item\" data-item=\"Fatture\" href=\"#\" data-value=\"4\" data-placeholder=\"Numero fattura\" data-icon=\"icon-book\">
                <i class=\"material-icons\">book</i></i> Fatture
              </a>
            </li>
            <li class=\"search-carts search-option\">
              <a class=\"dropdown-item\" data-item=\"Carrelli\" href=\"#\" data-value=\"5\" data-placeholder=\"ID carrello\" data-icon=\"icon-shopping-cart\">
                <i class=\"material-icons\">shopping_cart</i> Carrelli
              </a>
            </li>
            <li class=\"search-modules search-option\">
              <a class=\"dropdown-item\" data-item=\"Moduli\" href=\"#\" data-value=\"7\" data-placeholder=\"Nome modulo\" data-icon=\"icon-puzzle-piece\">
                <i class=\"material-icons\">view_module</i> Moduli
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class=\"input-group-addon search-bar\">
      <button type=\"submit\">CERCA<i class=\"material-icons\">search</i></button>
    </div>
  </div>
</form>

<script type=\"text/javascript\">
 \$(document).ready(function(){
  });
</script>
</div>


    <div class=\"component pull-md-right -norightmargin\"><div class=\"employee-dropdown dropdown\">
      <div class=\"img-circle person\" data-toggle=\"dropdown\">
      <i class=\"material-icons\">person</i>
    </div>
    <div class=\"dropdown-menu dropdown-menu-right p-a-1 m-r-2\">
    <div class=\"text-xs-center\">
      <img class=\"avatar img-circle\" src=\"http://profile.prestashop.com/info%40nextbox.it.jpg\" /><br>
      Nextbox Nextbox
    </div>
    <hr>
    <a class=\"employee-link\" href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminEmployees&amp;token=d666f957b501390be2d8425d46a683b2&amp;id_employee=1&amp;updateemployee\" target=\"_blank\">
      <i class=\"material-icons\">settings_applications</i> Il tuo profilo
    </a>
    <a class=\"employee-link m-t-1\" id=\"header_logout\" href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminLogin&amp;token=27a25620631b205c7a828d9e9b47ca84&amp;logout\">
      <i class=\"material-icons\">power_settings_new</i> Esci
    </a>
  </div>
</div>
</div>
          <div class=\"component pull-md-right\"><div class=\"notification-center dropdown\">
  <div class=\"notification dropdown-toggle\">
    <i class=\"material-icons\">notifications_none</i>
    <span id=\"notifications-total\" class=\"count\">0</span>
  </div>
  <div class=\"dropdown-menu dropdown-menu-right\">
    <div class=\"notifications\">
      <ul class=\"nav nav-tabs\" role=\"tablist\">
                          <li class=\"nav-item\">
            <a
              class=\"nav-link active\"
              id=\"orders-tab\"
              data-toggle=\"tab\"
              data-type=\"order\"
              href=\"#orders-notifications\"
              role=\"tab\"
            >
              Ordini<span id=\"_nb_new_orders_\"></span>
            </a>
          </li>
                                    <li class=\"nav-item\">
            <a
              class=\"nav-link \"
              id=\"customers-tab\"
              data-toggle=\"tab\"
              data-type=\"customer\"
              href=\"#customers-notifications\"
              role=\"tab\"
            >
              Clienti<span id=\"_nb_new_customers_\"></span>
            </a>
          </li>
                                    <li class=\"nav-item\">
            <a
              class=\"nav-link \"
              id=\"messages-tab\"
              data-toggle=\"tab\"
              data-type=\"customer_message\"
              href=\"#messages-notifications\"
              role=\"tab\"
            >
              Messaggi<span id=\"_nb_new_messages_\"></span>
            </a>
          </li>
                        </ul>

      <!-- Tab panes -->
      <div class=\"tab-content\">
                          <div class=\"tab-pane active empty\" id=\"orders-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              Per ora nessun nuovo ordine :(<br>
              Hai controllato i tuoi <strong><a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminCarts&token=72370ae57d3274ee3d20202bec538637&action=filterOnlyAbandonedCarts\">carrelli abbandonati</a></strong>?<br>
              Il tuo prossimo ordine potrebbe essere nascosto lì!
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                                    <div class=\"tab-pane  empty\" id=\"customers-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              Per ora nessun nuovo cliente :(<br>
              Hai pensato a vendere sul marketplace?
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                                    <div class=\"tab-pane  empty\" id=\"messages-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              Per ora non ci sono nuovi messaggi.<br>
              Tutto tempo risparmiato per qualcos'altro!
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                        </div>
    </div>
  </div>
</div>

  <script type=\"text/html\" id=\"order-notification-template\">
    <a class=\"notif\" href='order_url'>
      #_id_order_ -
      da <strong>_customer_name_</strong> (_iso_code_)_carrier_
      <strong class=\"pull-xs-right\">_total_paid_</strong>
    </a>
  </script>

  <script type=\"text/html\" id=\"customer-notification-template\">
    <a class=\"notif\" href='customer_url'>
      #_id_customer_ - <strong>_customer_name_</strong>_company_ - registrati <strong>_date_add_</strong>
    </a>
  </script>

  <script type=\"text/html\" id=\"message-notification-template\">
    <a class=\"notif\" href='message_url'>
    <span class=\"message-notification-status _status_\">
      <i class=\"material-icons\">fiber_manual_record</i> _status_
    </span>
      - <strong>_customer_name_</strong> (_company_) - <i class=\"material-icons\">access_time</i> _date_add_
    </a>
  </script>
</div>
        <div class=\"component pull-md-right\">  <div class=\"shop-list\">
    <a class=\"link\" href=\"http://localhost/\" target= \"_blank\">Floreiza</a>
  </div>
</div>
            

    

    
    
  </nav>
</header>

<nav class=\"nav-bar\">
  <ul class=\"main-menu\">

          
                
                
        
          <li class=\"link-levelone \" data-submenu=\"1\">
            <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminDashboard&amp;token=a425aecb4ed34fae2b99347b22f7dde7\" class=\"link\" >
              <i class=\"material-icons\">trending_up</i> <span>Pannello di controllo</span>
            </a>
          </li>

        
                
                                  
                
        
          <li class=\"category-title -active\" data-submenu=\"2\">
              <span class=\"title\">Sell</span>
          </li>

                          
                
                                
                <li class=\"link-levelone \" data-submenu=\"3\">
                  <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminOrders&amp;token=f6fe8a7d701144a1b39a62b5b3fb5ae8\" class=\"link\">
                    <i class=\"material-icons\">shopping_basket</i> <span>Ordini</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"4\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminOrders&amp;token=f6fe8a7d701144a1b39a62b5b3fb5ae8\" class=\"link\"> Ordini
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"5\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminInvoices&amp;token=4ae9737f66f16686b08c5fb316bf6e36\" class=\"link\"> Fatture
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"6\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminSlip&amp;token=b60ef3713ed195e0d1208ea12de8ebd4\" class=\"link\"> Note di credito
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"7\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminDeliverySlip&amp;token=574aa7703df00cd0927493ef9c44ee50\" class=\"link\"> Note di consegna
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"8\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminCarts&amp;token=72370ae57d3274ee3d20202bec538637\" class=\"link\"> Carrello
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone -active\" data-submenu=\"9\">
                  <a href=\"/admin631zqgp8w/index.php/product/catalog?_token=i4OrJhR5MTqS-LCyip5PoZFsrjZ6ELNFqMiE4MJ5NoQ\" class=\"link\">
                    <i class=\"material-icons\">store</i> <span>Catalogo</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo -active\" data-submenu=\"10\">
                              <a href=\"/admin631zqgp8w/index.php/product/catalog?_token=i4OrJhR5MTqS-LCyip5PoZFsrjZ6ELNFqMiE4MJ5NoQ\" class=\"link\"> Prodotti
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"11\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminCategories&amp;token=ff9f9cd4e863de4abd0b025a8bd9bc4c\" class=\"link\"> Categorie
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"12\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminTracking&amp;token=2447e51dc44445b61d9ff03486fe1c8e\" class=\"link\"> Monitoraggio
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"13\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminAttributesGroups&amp;token=f4c74d4a64921eb7c8ce5bee4740e645\" class=\"link\"> Attributi &amp; Features
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"16\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminManufacturers&amp;token=04344b7cb7ac7cf7483680d433410893\" class=\"link\"> Marchi e Fornitori
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"19\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminAttachments&amp;token=d2e9e4fd0c49299869514d4d84aec2a4\" class=\"link\"> Allegati
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"20\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminCartRules&amp;token=360d277e72b74d8e00ad22c0b40ceceb\" class=\"link\"> Discounts
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"23\">
                  <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminCustomers&amp;token=9a3f4deace6da3ca7c83eaa0953f7340\" class=\"link\">
                    <i class=\"material-icons\">account_circle</i> <span>Clienti</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"24\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminCustomers&amp;token=9a3f4deace6da3ca7c83eaa0953f7340\" class=\"link\"> Clienti
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"25\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminAddresses&amp;token=83097d1c3d79c15ea158f87cd9e2e82c\" class=\"link\"> Indirizzi
                              </a>
                            </li>

                                                                                                                          </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"27\">
                  <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminCustomerThreads&amp;token=439b0d380fa730ae91f1994ec90c2e0b\" class=\"link\">
                    <i class=\"material-icons\">chat</i> <span>Assistenza clienti</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"28\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminCustomerThreads&amp;token=439b0d380fa730ae91f1994ec90c2e0b\" class=\"link\"> Assistenza clienti
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"29\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminOrderMessage&amp;token=05f1a79de2ca25faa958490ed5a8b627\" class=\"link\"> Messaggi predefiniti
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"30\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminReturn&amp;token=81a59d08cca4f89023443fa0a2e63190\" class=\"link\"> Resi
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"31\">
                  <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminStats&amp;token=57f0bf7055150a5fa1ceebc75a35f176\" class=\"link\">
                    <i class=\"material-icons\">assessment</i> <span>Statistiche</span>
                  </a>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"41\">
              <span class=\"title\">Improve</span>
          </li>

                          
                
                                
                <li class=\"link-levelone \" data-submenu=\"42\">
                  <a href=\"/admin631zqgp8w/index.php/module/catalog?_token=i4OrJhR5MTqS-LCyip5PoZFsrjZ6ELNFqMiE4MJ5NoQ\" class=\"link\">
                    <i class=\"material-icons\">extension</i> <span>Modules</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"43\">
                              <a href=\"/admin631zqgp8w/index.php/module/catalog?_token=i4OrJhR5MTqS-LCyip5PoZFsrjZ6ELNFqMiE4MJ5NoQ\" class=\"link\"> Moduli e Servizi
                              </a>
                            </li>

                                                                                                                              
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"45\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminAddonsCatalog&amp;token=08a3a900120d653c078ab95f6fc4b974\" class=\"link\"> Strumenti di Compra-Vendita
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"46\">
                  <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminThemes&amp;token=3ae18808ef911dc3be3fe7ab244c2693\" class=\"link\">
                    <i class=\"material-icons\">desktop_mac</i> <span>Design</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"47\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminThemes&amp;token=3ae18808ef911dc3be3fe7ab244c2693\" class=\"link\"> Temi
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"48\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminThemesCatalog&amp;token=02cabd3cbb95b06033737f4f39a2881a\" class=\"link\"> Catalogo dei Temi
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"49\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminCmsContent&amp;token=b425e5df5632ad242127deb7177c8eb1\" class=\"link\"> Pagine
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"50\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminModulesPositions&amp;token=52a5e714feced429a44bf9393002dfa1\" class=\"link\"> Posizioni
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"51\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminImages&amp;token=de81a25193cc7a90190ad42a839f5923\" class=\"link\"> Image Settings
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"116\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminLinkWidget&amp;token=fc5c135d7634e6c186a686a41f98dd89\" class=\"link\"> Link Widget
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"52\">
                  <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminCarriers&amp;token=ea0b0e379dd2bbf40bdb3ff0421456db\" class=\"link\">
                    <i class=\"material-icons\">local_shipping</i> <span>Spedizione</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"53\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminCarriers&amp;token=ea0b0e379dd2bbf40bdb3ff0421456db\" class=\"link\"> Corrieri
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"54\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminShipping&amp;token=0e3a09dbceddcb3b33b9ee7e5e1ccabb\" class=\"link\"> Preferenze
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"55\">
                  <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminPayment&amp;token=33bd47e5c59b569aabc184240a6f7548\" class=\"link\">
                    <i class=\"material-icons\">payment</i> <span>Pagamento</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"56\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminPayment&amp;token=33bd47e5c59b569aabc184240a6f7548\" class=\"link\"> Metodi di Pagamento
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"57\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminPaymentPreferences&amp;token=51c20dd7ee5c189e9d1a53af98e82587\" class=\"link\"> Preferenze
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"58\">
                  <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminLocalization&amp;token=07baaccddab9ea61b409287761561e46\" class=\"link\">
                    <i class=\"material-icons\">language</i> <span>Internazionale</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"59\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminLocalization&amp;token=07baaccddab9ea61b409287761561e46\" class=\"link\"> Localizzazione
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"64\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminCountries&amp;token=7d2fa861152f9be3f56d353da5e1b45b\" class=\"link\"> Locations
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"68\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminTaxes&amp;token=e602e33c98115a138c69c5c45db5df95\" class=\"link\"> Tasse
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"71\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminTranslations&amp;token=ccb0fab065d27897909bf8dad5e24eb5\" class=\"link\"> Traduzioni
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"72\">
              <span class=\"title\">Configure</span>
          </li>

                          
                
                                
                <li class=\"link-levelone \" data-submenu=\"73\">
                  <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminPreferences&amp;token=ec846eb46f412acd15220e5bbbc42cc1\" class=\"link\">
                    <i class=\"material-icons\">settings</i> <span>Parametri Negozio</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"74\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminPreferences&amp;token=ec846eb46f412acd15220e5bbbc42cc1\" class=\"link\"> Generale
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"77\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminOrderPreferences&amp;token=c5ca1122b390fbf7a28c5732f61f546c\" class=\"link\"> Impostazioni Ordine
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"80\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminPPreferences&amp;token=974fa0285179fe65fd25c92518787264\" class=\"link\"> Prodotti
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"81\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminCustomerPreferences&amp;token=157c1b2ab86fd46685f9e2f2ec51f378\" class=\"link\"> Customer Settings
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"85\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminContacts&amp;token=46850f12676619964c58f642b9b57825\" class=\"link\"> Contatti
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"88\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminMeta&amp;token=9f58c7fe803a41cd65fd538b9a6fa604\" class=\"link\"> Traffico e SEO
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"92\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminSearchConf&amp;token=71cd622c3c18ac0c46ff0fc31fcf9ab7\" class=\"link\"> Cerca
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"95\">
                  <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminInformation&amp;token=3ac9a93997227309c8239577e8a4feb0\" class=\"link\">
                    <i class=\"material-icons\">settings_applications</i> <span>Parametri avanzati</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"96\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminInformation&amp;token=3ac9a93997227309c8239577e8a4feb0\" class=\"link\"> Informazione
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"97\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminPerformance&amp;token=cb889c251d4751b10ba6449eee047d86\" class=\"link\"> Prestazioni
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"98\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminAdminPreferences&amp;token=8ba9e85d9427a6f8bf73f1d278f7668b\" class=\"link\"> Amministrazione
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"99\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminEmails&amp;token=f945f3d9b6fe59c254ece378fcaace6c\" class=\"link\"> E-mail
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"100\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminImport&amp;token=650b21d3ec9c0b2cab314ec8d30919eb\" class=\"link\"> Importazione CSV
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"101\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminEmployees&amp;token=d666f957b501390be2d8425d46a683b2\" class=\"link\"> Dipendenti
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"105\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminRequestSql&amp;token=4fbde4a9744f363395e3f994a699214c\" class=\"link\"> Database
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"108\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminLogs&amp;token=bbc54985ca508d470d189162f0aa53a4\" class=\"link\"> Log
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"109\">
                              <a href=\"http://localhost/admin631zqgp8w/index.php?controller=AdminWebservice&amp;token=ab6930e034010320f6df4ceda7ffb7bc\" class=\"link\"> Webservice
                              </a>
                            </li>

                                                                                                                                                                            </ul>
                                    </li>
                          
        
            </ul>

  <span class=\"menu-collapse\">
    <i class=\"material-icons\">&#xE8EE;</i>
  </span>

  

</nav>


<div id=\"main-div\">

  
        
    <div class=\"content-div -notoolbar\">

      

      

      

      
      
      
      

      <div class=\"row \">
        <div class=\"col-xs-12\">
          <div id=\"ajax_confirmation\" class=\"alert alert-success\" style=\"display: none;\"></div>




  ";
        // line 917
        $this->displayBlock('content_header', $context, $blocks);
        // line 918
        echo "                 ";
        $this->displayBlock('content', $context, $blocks);
        // line 919
        echo "                 ";
        $this->displayBlock('content_footer', $context, $blocks);
        // line 920
        echo "                 ";
        $this->displayBlock('sidebar_right', $context, $blocks);
        // line 921
        echo "
        </div>
      </div>

    </div>

  
</div>


  <div id=\"footer\" class=\"bootstrap hide\">
<!--
  <div class=\"col-sm-2 hidden-xs\">
    <a href=\"http://www.prestashop.com/\" class=\"_blank\">PrestaShop&trade;</a>
    -
    <span id=\"footer-load-time\"><i class=\"icon-time\" title=\"Tempo di caricamento: \"></i> 1.258s</span>
  </div>

  <div class=\"col-sm-2 hidden-xs\">
    <div class=\"social-networks\">
      <a class=\"link-social link-twitter _blank\" href=\"https://twitter.com/PrestaShop\" title=\"Twitter\">
        <i class=\"icon-twitter\"></i>
      </a>
      <a class=\"link-social link-facebook _blank\" href=\"https://www.facebook.com/prestashop\" title=\"Facebook\">
        <i class=\"icon-facebook\"></i>
      </a>
      <a class=\"link-social link-github _blank\" href=\"https://www.prestashop.com/github\" title=\"Github\">
        <i class=\"icon-github\"></i>
      </a>
      <a class=\"link-social link-google _blank\" href=\"https://plus.google.com/+prestashop/\" title=\"Google\">
        <i class=\"icon-google-plus\"></i>
      </a>
    </div>
  </div>
  <div class=\"col-sm-5\">
    <div class=\"footer-contact\">
      <a href=\"http://www.prestashop.com/en/contact_us?utm_source=back-office&amp;utm_medium=footer&amp;utm_campaign=back-office-IT&amp;utm_content=download\" class=\"footer_link _blank\">
        <i class=\"icon-envelope\"></i>
        Contatto
      </a>
      /&nbsp;
      <a href=\"http://forge.prestashop.com/?utm_source=back-office&amp;utm_medium=footer&amp;utm_campaign=back-office-IT&amp;utm_content=download\" class=\"footer_link _blank\">
        <i class=\"icon-bug\"></i>
        Bug Tracker
      </a>
      /&nbsp;
      <a href=\"http://www.prestashop.com/forums/?utm_source=back-office&amp;utm_medium=footer&amp;utm_campaign=back-office-IT&amp;utm_content=download\" class=\"footer_link _blank\">
        <i class=\"icon-comments\"></i>
        Forum
      </a>
      /&nbsp;
      <a href=\"http://addons.prestashop.com/?utm_source=back-office&amp;utm_medium=footer&amp;utm_campaign=back-office-IT&amp;utm_content=download\" class=\"footer_link _blank\">
        <i class=\"icon-puzzle-piece\"></i>
        Addon
      </a>
      /&nbsp;
      <a href=\"http://www.prestashop.com/en/training-prestashop?utm_source=back-office&amp;utm_medium=footer&amp;utm_campaign=back-office-IT&amp;utm_content=download\" class=\"footer_link _blank\">
        <i class=\"icon-book\"></i>
        Addestramento
      </a>
                </div>
  </div>

  <div class=\"col-sm-3\">
    
  </div>

  <div id=\"go-top\" class=\"hide\"><i class=\"icon-arrow-up\"></i></div>
  -->
</div>



  <div class=\"bootstrap\">
    <div class=\"modal fade\" id=\"modal_addons_connect\" tabindex=\"-1\">
\t<div class=\"modal-dialog modal-md\">
\t\t<div class=\"modal-content\">
\t\t\t\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>
\t\t\t\t<h4 class=\"modal-title\"><i class=\"icon-puzzle-piece\"></i> <a target=\"_blank\" href=\"http://addons.prestashop.com/?utm_source=back-office&utm_medium=modules&utm_campaign=back-office-IT&utm_content=download\">PrestaShop Addons</a></h4>
\t\t\t</div>
\t\t\t
\t\t\t
<div class=\"modal-body\">
\t\t\t\t\t\t<!--start addons login-->
\t\t\t<form id=\"addons_login_form\" method=\"post\" >
\t\t\t\t<div>
\t\t\t\t\t<a href=\"https://addons.prestashop.com/it/login?email=info%40nextbox.it&amp;firstname=Nextbox&amp;lastname=Nextbox&amp;website=http%3A%2F%2Flocalhost%2F&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-IT&amp;utm_content=download#createnow\"><img class=\"img-responsive center-block\" src=\"/admin631zqgp8w/themes/default/img/prestashop-addons-logo.png\" alt=\"Logo PrestaShop Addons\"/></a>
\t\t\t\t\t<h3 class=\"text-center\">Collega il tuo negozio con il market di PrestaShop per importare autoamticamente tutti i tuoi acquisti di componenti aggiuntivi.</h3>
\t\t\t\t\t<hr />
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Non hai ancora un account?</h4>
\t\t\t\t\t\t<p class='text-justify'>Scopri la potenza di PrestaShop Addons! Esplora il Market Ufficiale di PrestaShop e troverai oltre 3500 moduli innovativi e temi che ottimizzano i tassi di conversione, incrementano il traffico, fidelizzano il cliente e massimizzano i tuoi ritorni</p>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Collegati con PrestaShop Addons</h4>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><i class=\"icon-user\"></i></span>
\t\t\t\t\t\t\t\t<input id=\"username_addons\" name=\"username_addons\" type=\"text\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><i class=\"icon-key\"></i></span>
\t\t\t\t\t\t\t\t<input id=\"password_addons\" name=\"password_addons\" type=\"password\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<a class=\"btn btn-link pull-right _blank\" href=\"//addons.prestashop.com/it/forgot-your-password\">Ho dimenticato la mia password</a>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"row row-padding-top\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<a class=\"btn btn-default btn-block btn-lg _blank\" href=\"https://addons.prestashop.com/it/login?email=info%40nextbox.it&amp;firstname=Nextbox&amp;lastname=Nextbox&amp;website=http%3A%2F%2Flocalhost%2F&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-IT&amp;utm_content=download#createnow\">
\t\t\t\t\t\t\t\tCrea un account
\t\t\t\t\t\t\t\t<i class=\"icon-external-link\"></i>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<button id=\"addons_login_button\" class=\"btn btn-primary btn-block btn-lg\" type=\"submit\">
\t\t\t\t\t\t\t\t<i class=\"icon-unlock\"></i> Accedi
\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div id=\"addons_loading\" class=\"help-block\"></div>

\t\t\t</form>
\t\t\t<!--end addons login-->
\t\t\t</div>


\t\t\t\t\t</div>
\t</div>
</div>
  </div>

";
        // line 1066
        $this->displayBlock('javascripts', $context, $blocks);
        $this->displayBlock('extra_javascripts', $context, $blocks);
        $this->displayBlock('translate_javascripts', $context, $blocks);
        echo "</body>
</html>";
    }

    // line 69
    public function block_stylesheets($context, array $blocks = array())
    {
    }

    public function block_extra_stylesheets($context, array $blocks = array())
    {
    }

    // line 917
    public function block_content_header($context, array $blocks = array())
    {
    }

    // line 918
    public function block_content($context, array $blocks = array())
    {
    }

    // line 919
    public function block_content_footer($context, array $blocks = array())
    {
    }

    // line 920
    public function block_sidebar_right($context, array $blocks = array())
    {
    }

    // line 1066
    public function block_javascripts($context, array $blocks = array())
    {
    }

    public function block_extra_javascripts($context, array $blocks = array())
    {
    }

    public function block_translate_javascripts($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "__string_template__3c395f5bcf0e015b8b622469e07bb427ea2d3d2d7cede17dddaa63e624e3843d";
    }

    public function getDebugInfo()
    {
        return array (  1145 => 1066,  1140 => 920,  1135 => 919,  1130 => 918,  1125 => 917,  1116 => 69,  1108 => 1066,  961 => 921,  958 => 920,  955 => 919,  952 => 918,  950 => 917,  98 => 69,  28 => 1,);
    }

    public function getSource()
    {
        return "";
    }
}
