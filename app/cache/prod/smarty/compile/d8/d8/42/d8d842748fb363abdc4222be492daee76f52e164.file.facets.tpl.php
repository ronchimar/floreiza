<?php /* Smarty version Smarty-3.1.19, created on 2017-02-27 18:44:45
         compiled from "/Applications/MAMP/htdocs/prestashop17/themes/floreiza/templates/catalog/_partials/facets.tpl" */ ?>
<?php /*%%SmartyHeaderCode:53572260858b4658d67f3e3-77793269%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd8d842748fb363abdc4222be492daee76f52e164' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop17/themes/floreiza/templates/catalog/_partials/facets.tpl',
      1 => 1484108868,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '53572260858b4658d67f3e3-77793269',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'facets' => 0,
    'facet' => 0,
    'filter' => 0,
    'js_enabled' => 0,
    'sort_order' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_58b4658d70f175_58554571',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58b4658d70f175_58554571')) {function content_58b4658d70f175_58554571($_smarty_tpl) {?><div id="search_filters">
  <?php  $_smarty_tpl->tpl_vars["facet"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["facet"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['facets']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["facet"]->key => $_smarty_tpl->tpl_vars["facet"]->value) {
$_smarty_tpl->tpl_vars["facet"]->_loop = true;
?>
    <?php if ($_smarty_tpl->tpl_vars['facet']->value['displayed']) {?>
      <section class="facet">
        <h1 class="h3"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['facet']->value['label'], ENT_QUOTES, 'UTF-8');?>
</h1>
        <?php if ($_smarty_tpl->tpl_vars['facet']->value['widgetType']!=='dropdown') {?>
          <ul>
            <?php  $_smarty_tpl->tpl_vars["filter"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["filter"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['facet']->value['filters']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["filter"]->key => $_smarty_tpl->tpl_vars["filter"]->value) {
$_smarty_tpl->tpl_vars["filter"]->_loop = true;
?>
              <?php if ($_smarty_tpl->tpl_vars['filter']->value['displayed']) {?>
                <li>
                  <label>
                    <?php if ($_smarty_tpl->tpl_vars['facet']->value['multipleSelectionAllowed']) {?>
                      <input
                        data-search-url="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter']->value['nextEncodedFacetsURL'], ENT_QUOTES, 'UTF-8');?>
"
                        <?php if (!$_smarty_tpl->tpl_vars['js_enabled']->value) {?> class="ps-shown-by-js" <?php }?>
                        type="checkbox"
                        <?php if ($_smarty_tpl->tpl_vars['filter']->value['active']) {?> checked <?php }?>
                      >
                    <?php } else { ?>
                      <input
                        data-search-url="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter']->value['nextEncodedFacetsURL'], ENT_QUOTES, 'UTF-8');?>
"
                        <?php if (!$_smarty_tpl->tpl_vars['js_enabled']->value) {?> class="ps-shown-by-js" <?php }?>
                        type="radio"
                        name="filter <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['facet']->value['label'], ENT_QUOTES, 'UTF-8');?>
"
                        <?php if ($_smarty_tpl->tpl_vars['filter']->value['active']) {?> checked <?php }?>
                      >
                    <?php }?>

                    <a
                      rel="nofollow"
                      href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter']->value['nextEncodedFacetsURL'], ENT_QUOTES, 'UTF-8');?>
"
                      class="js-search-link <?php if ($_smarty_tpl->tpl_vars['filter']->value['active']) {?> active <?php }?>"
                    >
                      <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter']->value['label'], ENT_QUOTES, 'UTF-8');?>

                      <?php if ($_smarty_tpl->tpl_vars['filter']->value['magnitude']) {?>
                        <span class="magnitude"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter']->value['magnitude'], ENT_QUOTES, 'UTF-8');?>
</span>
                      <?php }?>
                    </a>
                  </label>
                </li>
              <?php }?>
            <?php } ?>
          </ul>
        <?php } else { ?>
          <form>
            <input type="hidden" name="order" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sort_order']->value, ENT_QUOTES, 'UTF-8');?>
">
            <select name="q">
              <option disabled selected hidden><?php echo smartyTranslate(array('s'=>'(no filter)','d'=>'Shop.Theme'),$_smarty_tpl);?>
</option>
              <?php  $_smarty_tpl->tpl_vars["filter"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["filter"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['facet']->value['filters']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["filter"]->key => $_smarty_tpl->tpl_vars["filter"]->value) {
$_smarty_tpl->tpl_vars["filter"]->_loop = true;
?>
                <?php if ($_smarty_tpl->tpl_vars['filter']->value['displayed']) {?>
                  <option
                    <?php if ($_smarty_tpl->tpl_vars['filter']->value['active']) {?>
                      selected
                      value="<?php echo htmlspecialchars($_GET['q'], ENT_QUOTES, 'UTF-8');?>
"
                    <?php } else { ?>
                      value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter']->value['nextEncodedFacets'], ENT_QUOTES, 'UTF-8');?>
"
                    <?php }?>
                  >
                    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter']->value['label'], ENT_QUOTES, 'UTF-8');?>

                    <?php if ($_smarty_tpl->tpl_vars['filter']->value['magnitude']) {?>
                      (<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter']->value['magnitude'], ENT_QUOTES, 'UTF-8');?>
)
                    <?php }?>
                  </option>
                <?php }?>
              <?php } ?>
            </select>
            <?php if (!$_smarty_tpl->tpl_vars['js_enabled']->value) {?>
              <button class="ps-hidden-by-js" type="submit">
                <?php echo smartyTranslate(array('s'=>'Filter','d'=>'Shop.Theme.Actions'),$_smarty_tpl);?>

              </button>
            <?php }?>
          </form>
        <?php }?>
      </section>
    <?php }?>
  <?php } ?>
</div>
<?php }} ?>
