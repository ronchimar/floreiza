<?php /* Smarty version Smarty-3.1.19, created on 2017-02-27 18:32:45
         compiled from "/Applications/MAMP/htdocs/prestashop17/modules/relatedfree/views/templates/admin/tabs.tpl" */ ?>
<?php /*%%SmartyHeaderCode:133197392758b462bd519c97-93259591%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '87f637b28e90a5510a4c1c8b6148565d705b34a5' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop17/modules/relatedfree/views/templates/admin/tabs.tpl',
      1 => 1478472938,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '133197392758b462bd519c97-93259591',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'related_category' => 0,
    'related_nb' => 0,
    'related_link' => 0,
    'id_product' => 0,
    'secure_key' => 0,
    'physical_uri' => 0,
    'virtual_uri' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_58b462bd55b105_68513304',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58b462bd55b105_68513304')) {function content_58b462bd55b105_68513304($_smarty_tpl) {?>

<div class="panel " id="relatedFreeDiv">
    <input type="hidden" name="relatedfree" value="1"/>
    <h2 class="tab"><?php echo smartyTranslate(array('s'=>'Related products free','d'=>'Module.RelatedFree.Admin'),$_smarty_tpl);?>
</h2>
    <div class="separation"></div>
    <table>
        <td><img src="../../../../modules/relatedfree/related-pro.png" class="img-responsive"/></td>
        <td style="width:20px;"></td>
        <td>
            1. <?php echo smartyTranslate(array('s'=>'Want much more appearance options?','d'=>'Module.RelatedFree.Admin'),$_smarty_tpl);?>
<br/>
            2. <?php echo smartyTranslate(array('s'=>'Want to display products in random order?','d'=>'Module.RelatedFree.Admin'),$_smarty_tpl);?>
<br/>
            3. <?php echo smartyTranslate(array('s'=>'Want to display custom selected products only?','d'=>'Module.RelatedFree.Admin'),$_smarty_tpl);?>
<br/>
            4. <?php echo smartyTranslate(array('s'=>'Want to display list of products in "tabs" section?','d'=>'Module.RelatedFree.Admin'),$_smarty_tpl);?>
<br/>
            5. <?php echo smartyTranslate(array('s'=>'Want to display unlimited number of blocks?','d'=>'Module.RelatedFree.Admin'),$_smarty_tpl);?>
<br/>
            6. <?php echo smartyTranslate(array('s'=>'Want to create related products carousell?','d'=>'Module.RelatedFree.Admin'),$_smarty_tpl);?>
<br/>
            7. <?php echo smartyTranslate(array('s'=>'Want the best related products tool?','d'=>'Module.RelatedFree.Admin'),$_smarty_tpl);?>
<br/>
            <span style="font-size:18px;"><?php echo smartyTranslate(array('s'=>'check','d'=>'Module.RelatedFree.Admin'),$_smarty_tpl);?>
 <a href="http://mypresta.eu/modules/front-office-features/related-products-pro.html" target="_blank">related products pro</a> <?php echo smartyTranslate(array('s'=>'module','d'=>'Module.RelatedFree.Admin'),$_smarty_tpl);?>
.</span>
        </td>
    </table>
    <h2 class="tab"><?php echo smartyTranslate(array('s'=>'Module Settings','d'=>'Module.RelatedFree.Admin'),$_smarty_tpl);?>
</h2>
    <fieldset style="border:none;">
        <div class="alert alert-info">
            <i class="material-icons">help</i>
            <p class="alert-text">
                <?php echo smartyTranslate(array('s'=>'Category ID','d'=>'Module.RelatedFree.Admin'),$_smarty_tpl);?>
<br>
                <a href="https://mypresta.eu/en/art/basic-tutorials/prestashop-how-to-get-category-id.html" target="_blank"><?php echo smartyTranslate(array('s'=>'How to get category ID ?','d'=>'Module.RelatedFree.Admin'),$_smarty_tpl);?>
</a>
            </p>
        </div>
        <div class="form_block">
            <div>
                <?php echo smartyTranslate(array('s'=>'Category ID','d'=>'Module.RelatedFree.Admin'),$_smarty_tpl);?>

                <input type="text" name="related_category" id="related_category" class="ex_search form-control" value="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['related_category']->value,'int','utf-8');?>
"/>
            </div>
        </div>

        <div class="alert alert-info">
            <i class="material-icons">help</i>
            <p class="alert-text">
                <?php echo smartyTranslate(array('s'=>'Number of products','d'=>'Module.RelatedFree.Admin'),$_smarty_tpl);?>
<br>
                <?php echo smartyTranslate(array('s'=>'Define how many products will appear inside product block','d'=>'Module.RelatedFree.Admin'),$_smarty_tpl);?>

            </p>
        </div>
        <div class="form_block">
            <div>
                <?php echo smartyTranslate(array('s'=>'Number of products ','d'=>'Module.RelatedFree.Admin'),$_smarty_tpl);?>

                <input type="text" name="related_nb" id="related_nb" value="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['related_nb']->value,'int','utf-8');?>
" class="ex_search form-control"/>
            </div>
        </div>


        <div class="form_block">
            <div>
                <?php echo smartyTranslate(array('s'=>'Add category link to title of block','d'=>'Module.RelatedFree.Admin'),$_smarty_tpl);?>

                <select name="related_link" id="related_link">
                    <option value="1" <?php if ($_smarty_tpl->tpl_vars['related_link']->value==1) {?>selected<?php }?>><?php echo smartyTranslate(array('s'=>'Yes','d'=>'Module.RelatedFree.Admin'),$_smarty_tpl);?>
</option>
                    <option value="0" <?php if ($_smarty_tpl->tpl_vars['related_link']->value==0) {?>selected<?php }?>><?php echo smartyTranslate(array('s'=>'No','d'=>'Module.RelatedFree.Admin'),$_smarty_tpl);?>
</option>
                </select>
            </div>
        </div>
        <div class="separation"></div>
        <button onClick="submitRelatedFreeForm();" name="submitAddproductAndStay" class="btn btn-default pull-right"><i class="process-icon-save"></i><?php echo smartyTranslate(array('s'=>'Save module settings','d'=>'Module.RelatedFree.Admin'),$_smarty_tpl);?>
</button>
        <div class="clear">&nbsp;</div>
        <script>
            
                function submitRelatedFreeForm(){
                    var order = "&id_product=<?php echo $_smarty_tpl->tpl_vars['id_product']->value;?>
&relatedfreesubmit=1&secure_key=<?php echo $_smarty_tpl->tpl_vars['secure_key']->value;?>
&related_nb="+$('#related_nb').val()+"&related_category="+$('#related_category').val()+"&related_link="+$('#related_link').val()+"&action=updateRelatedFree";
                    $.post("<?php echo $_smarty_tpl->tpl_vars['physical_uri']->value;?>
<?php echo $_smarty_tpl->tpl_vars['virtual_uri']->value;?>
modules/relatedfree/ajax_relatedfree.php?secure_key=<?php echo $_smarty_tpl->tpl_vars['secure_key']->value;?>
", order);
                }
            
        </script>
        <style>
            
            #relatedFreeDiv {padding:10px;}
            #relatedFreeDiv .form_block {margin-bottom:20px;}
            
        </style>
        <iframe src="//apps.facepages.eu/somestuff/ps17.html" width="100%" height="150" border="0" style="margin-top:40px; margin-bottom: 50px; border:none; opacity:0.7"></iframe>
    </fieldset>
</div><?php }} ?>
