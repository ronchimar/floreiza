<?php /* Smarty version Smarty-3.1.19, created on 2017-02-27 18:29:26
         compiled from "module:relatedfree/views/templates/hook/productFooter.tpl" */ ?>
<?php /*%%SmartyHeaderCode:13677005958b461f6967487-60931751%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '122f7fe33bc74dd8be5b2b4c508718ace047638f' => 
    array (
      0 => 'module:relatedfree/views/templates/hook/productFooter.tpl',
      1 => 1488215766,
      2 => 'module',
    ),
  ),
  'nocache_hash' => '13677005958b461f6967487-60931751',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'related_link' => 0,
    'related_category' => 0,
    'id_lang' => 0,
    'link' => 0,
    'products' => 0,
    'product' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_58b461f69857e5_68302615',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58b461f69857e5_68302615')) {function content_58b461f69857e5_68302615($_smarty_tpl) {?>

<div id="featured-category-products-block-center" class="featured-products clearfix">
    <h1 class="products-section-title text-uppercase ">
        <?php if ($_smarty_tpl->tpl_vars['related_link']->value==1) {?><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getCategoryLink($_smarty_tpl->tpl_vars['related_category']->value,null,$_smarty_tpl->tpl_vars['id_lang']->value), ENT_QUOTES, 'UTF-8');?>
"><?php }?>
            <?php echo smartyTranslate(array('s'=>'Related products','d'=>'Module.RelatedFree.Shop'),$_smarty_tpl);?>

        <?php if ($_smarty_tpl->tpl_vars['related_link']->value==1) {?></a><?php }?>
    </h1>
    <?php if (isset($_smarty_tpl->tpl_vars['products']->value)&&$_smarty_tpl->tpl_vars['products']->value) {?>
        <div class="products">
            <?php  $_smarty_tpl->tpl_vars["product"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["product"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["product"]->key => $_smarty_tpl->tpl_vars["product"]->value) {
$_smarty_tpl->tpl_vars["product"]->_loop = true;
?>
                <?php echo $_smarty_tpl->getSubTemplate ("catalog/_partials/miniatures/product.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('product'=>$_smarty_tpl->tpl_vars['product']->value), 0);?>

            <?php } ?>
        </div>
    <?php } else { ?>
        <ul id="categoryfeatured" class="categoryfeatured tab-pane">
            <li class="alert alert-info"><?php echo smartyTranslate(array('s'=>'No related products at this time.','d'=>'Module.RelatedFree.Shop'),$_smarty_tpl);?>
</li>
        </ul>
    <?php }?>
</div><?php }} ?>
