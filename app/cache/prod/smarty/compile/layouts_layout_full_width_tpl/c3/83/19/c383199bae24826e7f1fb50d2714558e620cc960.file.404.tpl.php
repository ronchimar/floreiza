<?php /* Smarty version Smarty-3.1.19, created on 2017-02-27 18:44:52
         compiled from "/Applications/MAMP/htdocs/prestashop17/themes/floreiza/templates/errors/404.tpl" */ ?>
<?php /*%%SmartyHeaderCode:185331294958b465944db7f2-44224396%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c383199bae24826e7f1fb50d2714558e620cc960' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop17/themes/floreiza/templates/errors/404.tpl',
      1 => 1484108868,
      2 => 'file',
    ),
    '9b81058fd83d19571dae1cc38c988c02d282de39' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop17/themes/floreiza/templates/page.tpl',
      1 => 1484330934,
      2 => 'file',
    ),
    '87e4b980d3499b729b2505c5bb0091435e1f34e2' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop17/themes/floreiza/templates/layouts/layout-full-width.tpl',
      1 => 1487780047,
      2 => 'file',
    ),
    '30ad868f2e49bc150d5f7dddfb969fdf458b3ff0' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop17/themes/floreiza/templates/layouts/layout-both-columns.tpl',
      1 => 1487954061,
      2 => 'file',
    ),
    '11a695c4b26f07cdc6d1a78ee28ee5d0a8b4dc44' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop17/themes/floreiza/templates/_partials/stylesheets.tpl',
      1 => 1484108868,
      2 => 'file',
    ),
    '350b48bd46ddfb30afa53df43025dadbf04e9500' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop17/themes/floreiza/templates/_partials/javascript.tpl',
      1 => 1484108868,
      2 => 'file',
    ),
    'c245299ea8b8d729df2be60596c7053ee9c2da8a' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop17/themes/floreiza/templates/_partials/head.tpl',
      1 => 1484108868,
      2 => 'file',
    ),
    'd99f2464a44cd681e2e76360a0bae5de7dd31771' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop17/themes/floreiza/templates/catalog/_partials/product-activation.tpl',
      1 => 1484108868,
      2 => 'file',
    ),
    '944dac587873db3f2892762afa93a6c4abe08355' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop17/themes/floreiza/templates/_partials/breadcrumb.tpl',
      1 => 1488214094,
      2 => 'file',
    ),
    'e8b356b560ac8e8abb8d944ce65bf4b08dea9926' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop17/themes/floreiza/templates/_partials/header.tpl',
      1 => 1488214028,
      2 => 'file',
    ),
    '373a66eea170d1e8a3ef592f2a9056a577a5c6f7' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop17/themes/floreiza/templates/_partials/notifications.tpl',
      1 => 1484108868,
      2 => 'file',
    ),
    'be4eec13c87dc30c8f3ce9a57a769d1b5d1af1e3' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop17/themes/floreiza/templates/errors/not-found.tpl',
      1 => 1484108868,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '185331294958b465944db7f2-44224396',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'layout' => 0,
    'language' => 0,
    'page' => 0,
    'javascript' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_58b465946f03d8_78507569',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58b465946f03d8_78507569')) {function content_58b465946f03d8_78507569($_smarty_tpl) {?>
<!doctype html>
<html lang="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['language']->value['iso_code'], ENT_QUOTES, 'UTF-8');?>
">

  <head>
    
      <?php /*  Call merged included template "_partials/head.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('_partials/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0, '185331294958b465944db7f2-44224396');
content_58b46594538835_62740075($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "_partials/head.tpl" */?>
    
  </head>

  <body id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value['page_name'], ENT_QUOTES, 'UTF-8');?>
" class="<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['classnames'][0][0]->smartyClassnames($_smarty_tpl->tpl_vars['page']->value['body_classes']), ENT_QUOTES, 'UTF-8');?>
">

    <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayAfterBodyOpeningTag'),$_smarty_tpl);?>


    <main>
    	<div class="container">
      
        <?php /*  Call merged included template "catalog/_partials/product-activation.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('catalog/_partials/product-activation.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0, '185331294958b465944db7f2-44224396');
content_58b465945c4068_17088518($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "catalog/_partials/product-activation.tpl" */?>
      
      <header id="header">
        
          <?php /*  Call merged included template "_partials/header.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('_partials/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0, '185331294958b465944db7f2-44224396');
content_58b465945db513_42373677($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "_partials/header.tpl" */?>
        
      </header>
      
        <?php /*  Call merged included template "_partials/notifications.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('_partials/notifications.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0, '185331294958b465944db7f2-44224396');
content_58b46594614523_24732824($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "_partials/notifications.tpl" */?>
      
      <section>
        <div class="container">

          

          
  <div id="content-wrapper">
    

  <section id="main">

    
      <header class="page-header">
        
          <h1>
  <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value['title'], ENT_QUOTES, 'UTF-8');?>

</h1>
        
      </header>
    

    
  <?php /*  Call merged included template "errors/not-found.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('errors/not-found.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0, '185331294958b465944db7f2-44224396');
content_58b4659468bb23_05824103($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "errors/not-found.tpl" */?>


    
      <footer class="page-footer">
        
          <!-- Footer content -->
        
      </footer>
    

  </section>


  </div>


          
        </div>
      </section>

	<!-- bottom divider -->  
    <div class="container">
    	<section class="bottom-divider">

    	</section>
    </div>
    

	</div><!-- /.container -->
	
	<footer id="footer" class="grey-background">
		      <div class="container">
		          <div class="row footer-box">
		              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 footer-links">
		                  <a href="" title="Contatti">Contatti</a>
		                  <a href="" title="Franchising">Franchising</a>
		                  <a href="" title="Azienda">Azienda</a>
		              </div>
		              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 footer-links">
		                  <a href="" title="Condizioni generali">Condizioni generali</a>
		                  <a href="" title="Modalità di spedizione">Modalità di spedizione</a>
		                  <a href="" title="Modalità di pagamento">Modalità di pagamento</a>
		                  <a href="" title="Resi e rimborsi">Resi e rimborsi</a>
		              </div>              
		              <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 footer-links to-right">
		                  <p>Copyright &copy; Floreiza</p>
		                      <p>P.IVA 02022700971</p>
		                          <a href="" title="Privacy policy">Privacy policy</a>
		              </div>
		          </div>
		          
		          
		      </div>
	</footer>
	<!--
	<footer id="footer">
        
    </footer>
	-->
	</main>
	
	
    
      <?php /*  Call merged included template "_partials/javascript.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate("_partials/javascript.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('javascript'=>$_smarty_tpl->tpl_vars['javascript']->value['bottom']), 0, '185331294958b465944db7f2-44224396');
content_58b4659457f331_66903989($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "_partials/javascript.tpl" */?>
    

    <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayBeforeBodyClosingTag'),$_smarty_tpl);?>


  </body>

</html>
<?php }} ?>
<?php /* Smarty version Smarty-3.1.19, created on 2017-02-27 18:44:52
         compiled from "/Applications/MAMP/htdocs/prestashop17/themes/floreiza/templates/_partials/head.tpl" */ ?>
<?php if ($_valid && !is_callable('content_58b46594538835_62740075')) {function content_58b46594538835_62740075($_smarty_tpl) {?><meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">


  <title><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value['meta']['title'], ENT_QUOTES, 'UTF-8');?>
</title>
  <meta name="description" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value['meta']['description'], ENT_QUOTES, 'UTF-8');?>
">
  <meta name="keywords" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value['meta']['keywords'], ENT_QUOTES, 'UTF-8');?>
">
  <?php if ($_smarty_tpl->tpl_vars['page']->value['meta']['robots']!=='index') {?>
    <meta name="robots" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value['meta']['robots'], ENT_QUOTES, 'UTF-8');?>
">
  <?php }?>
  <?php if ($_smarty_tpl->tpl_vars['page']->value['canonical']) {?>
    <link rel="canonical" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value['canonical'], ENT_QUOTES, 'UTF-8');?>
">
  <?php }?>


<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="icon" type="image/vnd.microsoft.icon" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['favicon'], ENT_QUOTES, 'UTF-8');?>
?<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['favicon_update_time'], ENT_QUOTES, 'UTF-8');?>
">
<link rel="shortcut icon" type="image/x-icon" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['favicon'], ENT_QUOTES, 'UTF-8');?>
?<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['favicon_update_time'], ENT_QUOTES, 'UTF-8');?>
">


  <?php /*  Call merged included template "_partials/stylesheets.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate("_partials/stylesheets.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('stylesheets'=>$_smarty_tpl->tpl_vars['stylesheets']->value), 0, '185331294958b465944db7f2-44224396');
content_58b4659455e3d2_48798764($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "_partials/stylesheets.tpl" */?>



  <?php /*  Call merged included template "_partials/javascript.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate("_partials/javascript.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('javascript'=>$_smarty_tpl->tpl_vars['javascript']->value['head'],'vars'=>$_smarty_tpl->tpl_vars['js_custom_vars']->value), 0, '185331294958b465944db7f2-44224396');
content_58b4659457f331_66903989($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "_partials/javascript.tpl" */?>



  <?php echo $_smarty_tpl->tpl_vars['HOOK_HEADER']->value;?>


<?php }} ?>
<?php /* Smarty version Smarty-3.1.19, created on 2017-02-27 18:44:52
         compiled from "/Applications/MAMP/htdocs/prestashop17/themes/floreiza/templates/_partials/stylesheets.tpl" */ ?>
<?php if ($_valid && !is_callable('content_58b4659455e3d2_48798764')) {function content_58b4659455e3d2_48798764($_smarty_tpl) {?><?php  $_smarty_tpl->tpl_vars['stylesheet'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['stylesheet']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['stylesheets']->value['external']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['stylesheet']->key => $_smarty_tpl->tpl_vars['stylesheet']->value) {
$_smarty_tpl->tpl_vars['stylesheet']->_loop = true;
?>
  <link rel="stylesheet" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['stylesheet']->value['uri'], ENT_QUOTES, 'UTF-8');?>
" type="text/css" media="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['stylesheet']->value['media'], ENT_QUOTES, 'UTF-8');?>
">
<?php } ?>

<?php  $_smarty_tpl->tpl_vars['stylesheet'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['stylesheet']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['stylesheets']->value['inline']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['stylesheet']->key => $_smarty_tpl->tpl_vars['stylesheet']->value) {
$_smarty_tpl->tpl_vars['stylesheet']->_loop = true;
?>
  <style>
    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['stylesheet']->value['content'], ENT_QUOTES, 'UTF-8');?>

  </style>
<?php } ?>
<?php }} ?>
<?php /* Smarty version Smarty-3.1.19, created on 2017-02-27 18:44:52
         compiled from "/Applications/MAMP/htdocs/prestashop17/themes/floreiza/templates/_partials/javascript.tpl" */ ?>
<?php if ($_valid && !is_callable('content_58b4659457f331_66903989')) {function content_58b4659457f331_66903989($_smarty_tpl) {?><?php  $_smarty_tpl->tpl_vars['js'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['js']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['javascript']->value['external']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['js']->key => $_smarty_tpl->tpl_vars['js']->value) {
$_smarty_tpl->tpl_vars['js']->_loop = true;
?>
  <script type="text/javascript" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['js']->value['uri'], ENT_QUOTES, 'UTF-8');?>
" <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['js']->value['attribute'], ENT_QUOTES, 'UTF-8');?>
></script>
<?php } ?>

<?php  $_smarty_tpl->tpl_vars['js'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['js']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['javascript']->value['inline']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['js']->key => $_smarty_tpl->tpl_vars['js']->value) {
$_smarty_tpl->tpl_vars['js']->_loop = true;
?>
  <script type="text/javascript">
    <?php echo $_smarty_tpl->tpl_vars['js']->value['content'];?>

  </script>
<?php } ?>

<?php if (isset($_smarty_tpl->tpl_vars['vars']->value)&&count($_smarty_tpl->tpl_vars['vars']->value)) {?>
  <script type="text/javascript">
    <?php  $_smarty_tpl->tpl_vars['var_value'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['var_value']->_loop = false;
 $_smarty_tpl->tpl_vars['var_name'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['vars']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['var_value']->key => $_smarty_tpl->tpl_vars['var_value']->value) {
$_smarty_tpl->tpl_vars['var_value']->_loop = true;
 $_smarty_tpl->tpl_vars['var_name']->value = $_smarty_tpl->tpl_vars['var_value']->key;
?>
    var <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['var_name']->value, ENT_QUOTES, 'UTF-8');?>
 = <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['json_encode'][0][0]->jsonEncode($_smarty_tpl->tpl_vars['var_value']->value);?>
;
    <?php } ?>
  </script>
<?php }?>
<?php }} ?>
<?php /* Smarty version Smarty-3.1.19, created on 2017-02-27 18:44:52
         compiled from "/Applications/MAMP/htdocs/prestashop17/themes/floreiza/templates/catalog/_partials/product-activation.tpl" */ ?>
<?php if ($_valid && !is_callable('content_58b465945c4068_17088518')) {function content_58b465945c4068_17088518($_smarty_tpl) {?><?php if ($_smarty_tpl->tpl_vars['page']->value['admin_notifications']) {?>
  <div role="alert">
    <?php  $_smarty_tpl->tpl_vars['notif'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['notif']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['page']->value['admin_notifications']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['notif']->key => $_smarty_tpl->tpl_vars['notif']->value) {
$_smarty_tpl->tpl_vars['notif']->_loop = true;
?>
      <p><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['notif']->value['message'], ENT_QUOTES, 'UTF-8');?>
</p>
    <?php } ?>
  </div>
<?php }?>
<?php }} ?>
<?php /* Smarty version Smarty-3.1.19, created on 2017-02-27 18:44:52
         compiled from "/Applications/MAMP/htdocs/prestashop17/themes/floreiza/templates/_partials/header.tpl" */ ?>
<?php if ($_valid && !is_callable('content_58b465945db513_42373677')) {function content_58b465945db513_42373677($_smarty_tpl) {?>

  <div class="header-top">
    <div class="container">
    	
    	<!-- top header -->    
          <div class="row top-header-box">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 to-left">
	       	  <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayNav1'),$_smarty_tpl);?>

            </div>
            <!-- Controlli utente (lingua, carrello, valuta, login) -->
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 to-right display_nav">
              <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayNav'),$_smarty_tpl);?>

            </div>             
          </div>
      
      <!-- social header -->    
          <div class="row top-header-box">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 to-left">
              &nbsp;
            </div>   
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 to-right">
              sociala icons
            </div>             
          </div>      
      
      <!-- logo header -->
      <div class="header-logo-box" id="_desktop_logo">
	  	<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url'], ENT_QUOTES, 'UTF-8');?>
" title="Floreiza Shop"><img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['logo'], ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['name'], ENT_QUOTES, 'UTF-8');?>
"/></a>
      </div>
      

	  <!-- main desktop menu -->
      
      <div class="main-desktop-menu-box">
          <a href="" title="Floreiza shop">shop</a>
          <a href="" title="Floreiza lookbook">lookbook</a>
          <a href="" title="Floreiza stores">stores</a>
          <a href="" title="Floreiza news">news</a>
      </div>
        
    
      <div id="mobile_top_menu_wrapper" class="row hidden-md-up" style="display:none;">
        <div class="js-top-menu mobile" id="_mobile_top_menu"></div>
        <div class="js-top-menu-bottom">
          <div id="_mobile_currency_selector"></div>
          <div id="_mobile_language_selector"></div>
          <div id="_mobile_contact_link"></div>
        </div>
      </div>
  	
  		
      <div class="row breadcrumb_search">
      	<!-- breadcumb -->     
	  	  
	        <?php /*  Call merged included template "_partials/breadcrumb.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('_partials/breadcrumb.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0, '185331294958b465944db7f2-44224396');
content_58b465945f0071_65323137($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "_partials/breadcrumb.tpl" */?>
	      
	      
	      <div class="col-sm-6">
	      	<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['widget'][0][0]->smartyWidget(array('name'=>"ps_searchbar"),$_smarty_tpl);?>

	      </div>
      </div> 
  	  
	  <!-- submenu -->
	  <div class="main-page-menu">
	  <!-- Hook displayTop (Menu categorie)-->
	        
	 		  <a href="/it/3-abiti" title="">Abiti</a>
	          <a href="" title="">Camicie</a> 
	          <a href="" title="">Cappotti</a> 
	          <a href="" title="">Casacche</a> 
	          <a href="" title="">Fasce Giacche</a>
	          <a href="" title="">Giubbotti</a>
	          <a href="" title="">Gilet</a>
	          <a href="" title="">Gonne</a>
	          <a href="" title="">Maglie</a>
	          <a href="" title="">Pantaloni & Jeans</a>
	          <a href="" title="">Salopette & Tute</a>
	          <a href="" title="">T-shirt Top</a>
	  </div> 
	  
	  <div class="main-desktop-menu-box">
	  
	  </div>
  	</div><!-- / .container -->
  </div>







<?php }} ?>
<?php /* Smarty version Smarty-3.1.19, created on 2017-02-27 18:44:52
         compiled from "/Applications/MAMP/htdocs/prestashop17/themes/floreiza/templates/_partials/breadcrumb.tpl" */ ?>
<?php if ($_valid && !is_callable('content_58b465945f0071_65323137')) {function content_58b465945f0071_65323137($_smarty_tpl) {?><nav data-depth="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['breadcrumb']->value['count'], ENT_QUOTES, 'UTF-8');?>
" class="hidden-sm-down col-sm-6">
  <ol class="breadcrumb">
  	<li class="breadcrumb-item">
  		<a itemprop="item" href="http://www.floreiza.it" >
          <span itemprop="name">Home</span>
       </a>
  	</li>
    <?php  $_smarty_tpl->tpl_vars['path'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['path']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['breadcrumb']->value['links']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['path']->key => $_smarty_tpl->tpl_vars['path']->value) {
$_smarty_tpl->tpl_vars['path']->_loop = true;
?>
      <li class="breadcrumb-item">
        <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['path']->value['url'], ENT_QUOTES, 'UTF-8');?>
">
          <span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['path']->value['title'], ENT_QUOTES, 'UTF-8');?>
</span>
        </a>
      </li>
    <?php } ?>
  </ol>
</nav>
<?php }} ?>
<?php /* Smarty version Smarty-3.1.19, created on 2017-02-27 18:44:52
         compiled from "/Applications/MAMP/htdocs/prestashop17/themes/floreiza/templates/_partials/notifications.tpl" */ ?>
<?php if ($_valid && !is_callable('content_58b46594614523_24732824')) {function content_58b46594614523_24732824($_smarty_tpl) {?><aside id="notifications">

  <?php if ($_smarty_tpl->tpl_vars['notifications']->value['error']) {?>
    <article class="notification notification-danger" role="alert" data-alert="danger">
      <ul>
        <?php  $_smarty_tpl->tpl_vars['notif'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['notif']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['notifications']->value['error']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['notif']->key => $_smarty_tpl->tpl_vars['notif']->value) {
$_smarty_tpl->tpl_vars['notif']->_loop = true;
?>
          <li><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['notif']->value, ENT_QUOTES, 'UTF-8');?>
</li>
        <?php } ?>
      </ul>
    </article>
  <?php }?>

  <?php if ($_smarty_tpl->tpl_vars['notifications']->value['warning']) {?>
    <article class="notification notification-warning" role="alert" data-alert="warning">
      <ul>
        <?php  $_smarty_tpl->tpl_vars['notif'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['notif']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['notifications']->value['warning']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['notif']->key => $_smarty_tpl->tpl_vars['notif']->value) {
$_smarty_tpl->tpl_vars['notif']->_loop = true;
?>
          <li><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['notif']->value, ENT_QUOTES, 'UTF-8');?>
</li>
        <?php } ?>
      </ul>
    </article>
  <?php }?>

  <?php if ($_smarty_tpl->tpl_vars['notifications']->value['success']) {?>
    <article class="notification notification-success" role="alert" data-alert="success">
      <ul>
        <?php  $_smarty_tpl->tpl_vars['notif'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['notif']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['notifications']->value['success']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['notif']->key => $_smarty_tpl->tpl_vars['notif']->value) {
$_smarty_tpl->tpl_vars['notif']->_loop = true;
?>
          <li><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['notif']->value, ENT_QUOTES, 'UTF-8');?>
</li>
        <?php } ?>
      </ul>
    </article>
  <?php }?>

  <?php if ($_smarty_tpl->tpl_vars['notifications']->value['info']) {?>
    <article class="notification notification-info" role="alert" data-alert="info">
      <ul>
        <?php  $_smarty_tpl->tpl_vars['notif'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['notif']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['notifications']->value['info']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['notif']->key => $_smarty_tpl->tpl_vars['notif']->value) {
$_smarty_tpl->tpl_vars['notif']->_loop = true;
?>
          <li><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['notif']->value, ENT_QUOTES, 'UTF-8');?>
</li>
        <?php } ?>
      </ul>
    </article>
  <?php }?>

</aside>
<?php }} ?>
<?php /* Smarty version Smarty-3.1.19, created on 2017-02-27 18:44:52
         compiled from "/Applications/MAMP/htdocs/prestashop17/themes/floreiza/templates/errors/not-found.tpl" */ ?>
<?php if ($_valid && !is_callable('content_58b4659468bb23_05824103')) {function content_58b4659468bb23_05824103($_smarty_tpl) {?><section id="content" class="page-content page-not-found">
  <p><?php echo smartyTranslate(array('s'=>'Sorry for the inconvenience.','d'=>'Shop.Theme'),$_smarty_tpl);?>
</p>

  
    <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displaySearch'),$_smarty_tpl);?>

  

  <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayNotFound'),$_smarty_tpl);?>

</section>
<?php }} ?>
