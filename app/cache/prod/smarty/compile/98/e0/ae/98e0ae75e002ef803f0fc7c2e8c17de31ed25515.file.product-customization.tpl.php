<?php /* Smarty version Smarty-3.1.19, created on 2017-02-27 18:05:22
         compiled from "/Applications/MAMP/htdocs/prestashop17/themes/floreiza/templates/catalog/_partials/product-customization.tpl" */ ?>
<?php /*%%SmartyHeaderCode:115651936358b45c526d7090-12731012%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '98e0ae75e002ef803f0fc7c2e8c17de31ed25515' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestashop17/themes/floreiza/templates/catalog/_partials/product-customization.tpl',
      1 => 1484108868,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '115651936358b45c526d7090-12731012',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'product' => 0,
    'configuration' => 0,
    'urls' => 0,
    'customizations' => 0,
    'field' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_58b45c52701bf0_51262048',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58b45c52701bf0_51262048')) {function content_58b45c52701bf0_51262048($_smarty_tpl) {?><?php if ($_smarty_tpl->tpl_vars['product']->value['is_customizable']) {?>
  <section class="product-customization">
    <?php if (!$_smarty_tpl->tpl_vars['configuration']->value['is_catalog']) {?>
      <h3><?php echo smartyTranslate(array('s'=>'Product customization'),$_smarty_tpl);?>
</h3>
      <form method="post" action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['current_url'], ENT_QUOTES, 'UTF-8');?>
" enctype="multipart/form-data">
        <ul>
          <?php  $_smarty_tpl->tpl_vars["field"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["field"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['customizations']->value['fields']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["field"]->key => $_smarty_tpl->tpl_vars["field"]->value) {
$_smarty_tpl->tpl_vars["field"]->_loop = true;
?>
            <li>
              <label><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value['label'], ENT_QUOTES, 'UTF-8');?>
</label>
              <?php if ($_smarty_tpl->tpl_vars['field']->value['type']=='text') {?>
                <textarea <?php if ($_smarty_tpl->tpl_vars['field']->value['required']) {?> required <?php }?> name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value['input_name'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value['text'], ENT_QUOTES, 'UTF-8');?>
</textarea>
              <?php } elseif ($_smarty_tpl->tpl_vars['field']->value['type']=='image') {?>
                <?php if ($_smarty_tpl->tpl_vars['field']->value['is_customized']) {?>
                  <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value['image']['small']['url'], ENT_QUOTES, 'UTF-8');?>
">
                  <a class="remove-image" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value['remove_image_url'], ENT_QUOTES, 'UTF-8');?>
" rel="nofollow"><?php echo smartyTranslate(array('s'=>'Remove Image'),$_smarty_tpl);?>
</a>
                <?php }?>
                <input <?php if ($_smarty_tpl->tpl_vars['field']->value['required']) {?> required <?php }?> type="file" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value['input_name'], ENT_QUOTES, 'UTF-8');?>
">
              <?php }?>
            </li>
          <?php } ?>
        </ul>
        <button name="submitCustomizedData"><?php echo smartyTranslate(array('s'=>'Save Customization'),$_smarty_tpl);?>
</button>
      </form>
    <?php }?>
  </section>
<?php }?>
<?php }} ?>
