#VERSIONE OK (ps_attribute.color sara' da indicizzare, per migliorare le perfomance):

SELECT p.*, a.id_attribute_group, a.color, a.position, pac.id_product_attribute as id_product_attribute_floreiza, pa.id_product_attribute
FROM ps_product_attribute_combination AS pac            
LEFT JOIN ps_product_attribute AS pa ON pac.id_product_attribute = pa.id_product_attribute
LEFT JOIN ps_attribute AS a ON pac.id_attribute = a.id_attribute 
LEFT JOIN ps_product AS p ON pa.id_product = p.id_product
WHERE a.id_attribute_group = 3
GROUP BY a.color, p.id_product 
