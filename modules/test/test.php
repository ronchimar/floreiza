<?php
if (!defined('_PS_VERSION_'))
{
  exit;
}

class Test extends Module
{
  public function __construct()
  {
    $this->name = 'test';
    $this->tab = 'front_office_features';
    $this->version = '1.0.0';
    $this->author = $this->l('Il nome autoresco');
    $this->need_instance = 0;
    $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);
    $this->bootstrap = true;

    parent::__construct();

    $this->displayName = $this->l('Modulo di test');
    $this->description = $this->l('Descrizione del simpaticissimo modulo di test.');

    $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

    if (!Configuration::get('TEST'))
      $this->warning = $this->l('No name provided');
  }
  
  public function install()
  {
  	/*
	  if (Shop::isFeatureActive())
    Shop::setContext(Shop::CONTEXT_ALL);
	*/
	
	  return parent::install() &&
	    $this->registerHook('leftColumn') &&
	    Configuration::updateValue('MYMODULE_NAME', 'my friend');
  }

/*  
  public function getContent()
  {
  	$output = null;

    if (Tools::isSubmit('submit'.$this->name))
    {
        $my_module_name = strval(Tools::getValue('TEST_NAME'));
        if (!$my_module_name
          || empty($my_module_name)
          || !Validate::isGenericName($my_module_name))
            $output .= $this->displayError($this->l('Invalid Configuration value'));
        else
        {
            Configuration::updateValue('TEST_NAME', $my_module_name);
            $output .= $this->displayConfirmation($this->l('Settings updated'));
        }
    }
    return $output.$this->displayForm();
  }
  
public function displayForm()
{
    // Get default language
    $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

    // Init Fields form array
    $fields_form[0]['form'] = array(
        'legend' => array(
            'title' => $this->l('Settings'),
        ),
        'input' => array(
            array(
                'type' => 'text',
                'label' => $this->l('Configuration value'),
                'name' => 'TEST_NAME',
                'size' => 20,
                'required' => true
            ),
            array(
                'type' => 'text',
                'label' => $this->l('Configuration value'),
                'name' => 'TEST_NAME2',
                'size' => 20,
                'required' => true
            ),
            array(
                'type' => 'textarea',
                'label' => $this->l('Cose varie'),
                'name' => 'TEST_NAME_AREA',
                'size' => 20,
                'required' => true
            )
        ),
        'submit' => array(
            'title' => $this->l('Save'),
            'class' => 'btn btn-default pull-right'
        )
    );

    $helper = new HelperForm();

    // Module, token and currentIndex
    $helper->module = $this;
    $helper->name_controller = $this->name;
    $helper->token = Tools::getAdminTokenLite('AdminModules');
    $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

    // Language
    $helper->default_form_language = $default_lang;
    $helper->allow_employee_form_lang = $default_lang;

    // Title and toolbar
    $helper->title = $this->displayName;
    $helper->show_toolbar = true;        // false -> remove toolbar
    $helper->toolbar_scroll = true;      // yes - > Toolbar is always visible on the top of the screen.
    $helper->submit_action = 'submit'.$this->name;
    $helper->toolbar_btn = array(
        'save' =>
        array(
            'desc' => $this->l('Save'),
            'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
            '&token='.Tools::getAdminTokenLite('AdminModules'),
        ),
        'back' => array(
            'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
            'desc' => $this->l('Back to list')
        )
    );

    // Load current value
    $helper->fields_value['TEST_NAME'] = Configuration::get('TEST_NAME');

    return $helper->generateForm($fields_form);
}
*/ 
}
